
public class Lab13 {

	public static void main(String[] args) {
		
		// TODO Auto-generated method stub
		int[][] arrayInt = { {1,2,3},{4,5,6} };

		int sum = 0;
		
		for (int[] row : arrayInt) {
		     for (int element: row) {
		       System.out.print(element);
		     }
		     System.out.println();
		}
		
		System.out.println();

		for (int row = 0; row < arrayInt.length; row++){
			for (int element = 0; element < arrayInt[row].length; element++){
				if( element ==  (arrayInt[row].length)-1 ){
					sum = sum+arrayInt[row][element];
				}
			}
		}

		System.out.println("Sum: "+sum);

	}
}
